import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

ReactDOM.render(<App />, document.getElementById("root"));

/**
 * When on Development, to prevent the page from reloading as we add classes
 * from Tailwind CSS, we set up Hot Reloading.
 */
if (module.hot) {
  module.hot.accept();
}

/** f you want your app to work offline and load faster, you can change
 * unregister() to register() below. Note this comes with some pitfalls.
 * Learn more about service workers: http://bit.ly/CRA-PWA
 */
serviceWorker.unregister();
