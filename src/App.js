import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
// import StateClass from './state/StateClass';
// import StateFunctional from './state/StateFunctional';
// import Login from "./forms/Login";
// import Register from "./forms/Register";

export default function App() {
  const [results, setResults] = useState([]);
  const [query, setQuery] = useState("Elon Musk");
  const [loading, setLoading] = useState(false);
  const searchInputRef = useRef();
  const [error, setError] = useState(null);

  /**
   * Pass the empty array as the second parameter to make sure useEffect
   * runs on componentDidMount but not on componentDidUpdate. With an item,
   * it will also run on updates to this item.
   */
  useEffect(() => {
    getResults();
  }, []);

  const getResults = async () => {
    setLoading(true);
    try {
      const response = await axios.get(
        `http://hn.algolia.com/api/v1/search?query=${query}`
      );
      setResults(response.data.hits);
    } catch (err) {
      setError(err);
    }
    setLoading(false);
  };

  const handleSearch = event => {
    /* preventDefault is sually used to prevent the page from reloading. */
    event.preventDefault();
    getResults();
  };

  const handleClearSearch = () => {
    setQuery("");
    searchInputRef.current.focus();
  };

  return (
    <div className="container max-w-md max-auto p-4 m-2 bg-purple-lightest shadow-lg rounded">
      <h1 className="text-grey-darkest font-thin">React Hooks News Feed</h1>
      <form onSubmit={handleSearch} className="mb-2">
        <input
          type="text"
          onChange={event => setQuery(event.target.value)}
          value={query}
          ref={searchInputRef}
          className="border p-1 rounded"
        />
        <button type="submit" className="bg-orange rounded m-1 p-1">
          Search
        </button>
        <button
          type="button"
          onClick={handleClearSearch}
          className="bg-teal text-white p-1 rounded"
        >
          Clear
        </button>
      </form>
      {loading ? (
        <div className="font-bold text-orange-dark">Loading results...</div>
      ) : (
        <ul className="list-reset leading-normal">
          {results.map(result => (
            <li key={result.objectID}>
              <a
                href={result.url}
                className="text-indigo-dark hover:text-indigo-darkest"
              >
                {result.title}
              </a>
            </li>
          ))}
        </ul>
      )}
      {error && <div className="text-red font-bold">{error.message}</div>}
    </div>
  );
}
