import React, { Component } from "react";

class StateClass extends Component {
  state = {
    count: 0,
    isOn: false,
    x: null,
    y: null
  };

  /* Need this effect on every render, so on mount but also on update. */
  componentDidMount() {
    document.title = `You have been clicked ${this.state.count} times`;
    window.addEventListener("mousemove", this.handleMouseMove);
  }
  componentDidUpdate() {
    document.title = `You have been clicked ${this.state.count} times`;
  }
  /* Need to remove listener when component unmounts to avoid memory leaks. */
  componentWillUnmount() {
    window.removeEventListener("mousemove", this.handleMouseMove);
  }

  handleMouseMove = event => {
    this.setState({
      x: event.pageX,
      y: event.pageY
    });
  };

  incrementCount = () => {
    this.setState(prevState => ({
      count: prevState.count + 1
    }));
  };

  toggleLight = () => {
    this.setState(prevState => ({
      isOn: !prevState.isOn
    }));
  };

  render() {
    return (
      /* React 16.7+ allows us to write React Fragments like this. */
      <>
        <h2>Counter</h2>
        <button onClick={this.incrementCount}>
          I was clicked {this.state.count} times!
        </button>
        <h2>Toggle Light</h2>
        <div
          style={{
            height: "50px",
            width: "50px",
            background: this.state.isOn ? "yellow" : "grey"
          }}
          onClick={this.toggleLight}
        />

        <h2>Mouse Position</h2>
        <p>X position: {this.state.x}</p>
        <p>Y position: {this.state.y}</p>
      </>
    );
  }
}

export default StateClass;
